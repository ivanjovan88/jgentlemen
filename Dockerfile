FROM openjdk:15-slim-buster

COPY target/jgentlemen-*-SNAPSHOT.jar /app.jar

CMD ["java", "-jar", "/app.jar"]
