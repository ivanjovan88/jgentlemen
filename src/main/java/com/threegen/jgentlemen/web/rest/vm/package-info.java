/**
 * View Models used by Spring MVC REST controllers.
 */
package com.threegen.jgentlemen.web.rest.vm;
